# Western Sydney University LaTeX style

## Files

The files included are:

- example.tex: an example set of slides to compile
- example.pdf: the generated PDF
- includes/wsy.sty: the Western Sydney Uni style file
- includes/WSU_Logo_RGB.pdf: the colour WSU logo
- includes/WSU_Logo_While.pdf: the white WSU logo
- includes/includes.linux.sty: Linux specific code
- includes/includes.osx.sty: OS X specific code
- includes/includes.windows.sty: Windows specific code


## Fonts

For the PDF to render correctly, please install the WSU fonts:

- Chronicle
- Gotham Narrow

both downloadable from the WSU Self Service App.

If the fonts are not available, edit the font names in the system
specific style file (e.g. includes/includes.linux.sty for Linux).


## Compiling

To use the system fonts, please compile the tex source file using
XeLaTeX, with the command:

```sh
xelatex --shell-escape example.tex
```
